<?php

/*
 * This file is part of shizzen/json-api.
 *
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Shizzen\JsonApi\{
    EloquentCollection,
    Http\Middleware\VerifyJsonApi,
    Http\Resources\ResourceCollection,
    Http\Controllers\ResourceController
};

return [
    'prefix' => 'json-api.',
    'models-namespace' => 'App\\',
    'router-prefix' => 'api',
    'router-method' => 'apiResource',
    'media' => [
        'name' => env('JSON_MEDIA_NAME', 'application/vnd.api+json'),
        'version' => env('JSON_MEDIA_VERSION', '1.0'),
        'allowed' => [
            'filter',
            'sort',
            'include',
            'page',
            'perPage',
            'fields',
        ],
    ],
    'broadcasting' => env('JSON_API_BROADCASTING', false),
    'middleware' => [
        VerifyJsonApi::class,
    ],
    'pagination' => [
        'perPage' => env('PAGINATION_PER_PAGE', 25),
    ],
    'relationships' => [
        'relationshipRelated',
        'relationshipIndex',
        'relationshipStore',
        'relationshipUpdate',
        'relationshipDestroy',
    ],
    'classes' => [
        'controller'            => ResourceController::class,
        'resource-collection'   => ResourceCollection::class,
        'eloquent-collection'   => EloquentCollection::class,
    ],
];

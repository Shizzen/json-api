<?php

return [
    'Attribute not found_message' => "':type' does not own a ':attribute' attribute.",

    'Bound key not found_message' => "':type' type does not own any bound key name. You need to assign an attribute name to the 'boundKeyName' property of this type.",

    'File not found_message' => "':file' does not exist.",

    'Header parameter_message' => "Parameters are forbidden on ':header' header.",

    'Immutable model_message' => "This model is immutable.",

    'Not allowed query parameter_message' => "':parameter' is not allowed in query string.",

    'Unknown relationship verb_message' => "':verb' is not a valid verb of ':model' ':relationship' relationship.",

    'Unparsable include_message' => "':relationship' from query string is unparsable.",
];

<?php

namespace Shizzen\JsonApi\Contracts;

interface HasBoundRelations
{
    /**
     * Get model alias.
     *
     * @return string
     */
    public static function getAlias();

    /**
     * Get fields which are always retrieved (you should include all foreign keys).
     *
     * @return array
     */
    public static function getRetrievedFields();

    /**
     * Get the attribute name used to index models on a bound relationship.
     *
     * @return string|null
     */
    public function getBoundKeyName();

    /**
     * Get the attribute used to index models on a bound relationship.
     *
     * @return mixed|null
     */
    public function getBoundKey();

    /**
     * Set the attribute used to index models on a bound relationship.
     *
     * @param  mixed  $key
     * @return $this
     */
    public function setBoundKey($key);

    /**
     * Get relations which are automatically merged to this model.
     *
     * @return array
     */
    public static function getBoundRelations();

    /**
     * Save all model's bound relations.
     *
     * @return $this
     */
    public function saveBoundRelations();

    /**
     * Set a given bound attribute on one of the model's relationships.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return $this
     */
    public function setBoundAttribute(string $key, $value);
}

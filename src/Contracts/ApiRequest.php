<?php

namespace Shizzen\JsonApi\Contracts;

use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;

interface ApiRequest extends ValidatesWhenResolved, Arrayable, ArrayAccess
{
	/**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize();

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules();

    /**
     * Get filters from query string.
     *
     * @return array
     */
    public function filters();

    /**
     * Get included relationships from query string.
     *
     * @return array
     */
    public function included();

    /**
     * Get sorts from query string.
     *
     * @return array
     */
    public function sorts();

    /**
     * Get page from query string.
     *
     * @return int|null
     */
    public function page();

    /**
     * Get items number per page from query string.
     *
     * @return int|null
     */
    public function perPage();

    /**
     * Get fields from query string.
     *
     * @return array
     */
    public function fields();

    /**
     * Get attributes from request body.
     *
     * @return array
     */
    public function attributes();

    /**
     * Get relationships from request body.
     *
     * @return array
     */
    public function relationships();

    /**
     * Get the Eloquent model bound to this request.
     *
     * @param  bool  $build
     * @return \Illuminate\Database\Eloquent\Model|string
     */
    public function model(bool $build = true);

    /**
     * Get the relationship present in current route.
     *
     * @return string|null
     */
    public function relationship();

    /**
     * Parse models present in request relationships.
     *
     * @return \Illuminate\Support\Collection
     */
    public function relatedModels();
}

<?php

namespace Shizzen\JsonApi\Contracts;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

interface JsonApiModel extends HasBoundRelations
{
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Shizzen\JsonApi\EloquentCollection
     */
    public function newCollection(array $models = []);

    /**
     * Router method used to generate this model routes (null cancels generation).
     *
     * @return string|null
     */
    public static function getRouterMethod();

    /**
     * Get the prefix applied to the model routes.
     *
     * @return string|null
     */
    public static function getRouterPrefix();

    /**
     * Router settings.
     * null cancels route generation, otherwise list middlewares
     *
     * @var array
     */
    public static function getRouterSettings();

    /**
     * The controller class used by this model routes.
     *
     * @return string
     */
    public static function getControllerClass();

    /**
     * Get model identifiers used by JSON:API.
     *
     * @return array
     */
    public function getApiIdentifiers();

    /**
     * Get meta data included into API resources.
     *
     * @return array
     */
    public function getApiMeta();

    /**
     * Get links to fetch the model or one of its relationships.
     *
     * @param  string|null  $relationshipName
     * @return array
     */
    public function getApiLinks(?string $relationshipName = null);

    /**
     * Get attributes according to JSON:API.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getApiAttributes();

    /**
     * Get all the loaded relations for the instance.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getApiRelations();

    /**
     * Scope a query to sort.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  array  $sorts
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMapSorts(EloquentBuilder $query, array $sorts = []);

    /**
     * Scope a query to filter.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  array  $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMapFilters(EloquentBuilder $query, array $filters = []);

    /**
     * Check whether an attribute or relationship is hidden.
     *
     * @param  string  $target
     * @return bool
     */
    public function isHidden(string $target);

    /**
     * Get all models from loaded relationships.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getRelatedModels();

    /**
     * Save model's relationships
     *
     * @param  iterable  $relatedMapping
     * @return $this
     */
    public function saveRelations(iterable $relatedMapping);

    /**
     * Save a model's relationship
     *
     * @param  string  $relationshipName
     * @param  \Illuminate\Database\Eloquent\Model|array  $relatedMapping
     * @return $this
     */
    public function saveRelation(string $relationshipName, $relatedModels);

    /**
     * Get the bound attributes that were changed.
     *
     * @return array
     */
    public function getBoundChanges();

    /**
     * Check whether the model got changed as wished.
     *
     * @param  array  $attributes
     * @return bool
     */
    public function wasChangedAsWished(array $attributes);

    /**
     * Check whether the model shall be broadcasted onto a public channel.
     *
     * @return bool
     */
    public function publicBroadcast();
}

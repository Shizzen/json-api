<?php

namespace Shizzen\JsonApi;

use Illuminate\Database\Eloquent\Builder;

class EloquentBuilder extends Builder
{
	use Traits\EloquentBuilder;
}
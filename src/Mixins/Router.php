<?php

namespace Shizzen\JsonApi\Mixins;

use Illuminate\Support\Arr;
use Illuminate\Routing\ResourceRegistrar;
use Shizzen\JsonApi\Exceptions\UnknownRelationshipVerbException;

class Router
{
    /**
     * Load all routes bound to this model.
     *
     * @param  string  $class
     * @param  array  $options
     * @return void
     */
	protected function resourceRelationship()
	{
		$relationshipDefaults   = config('json-api.relationships');
        $registrar              = app(ResourceRegistrar::class);

        return function (string $entityType, string $relationship, string $controller, array $methods = ['*'], array $middlewares = []) use ($relationshipDefaults, $registrar) {
        	$methods = $methods === ['*']
                ? $relationshipDefaults
                : array_intersect($methods, $relationshipDefaults);

            $uri        = $registrar->getResourceUri($entityType);
            $base       = $registrar->getResourceWildcard(last(explode('.', $entityType)));

            $relatedUri         = $uri.'/{'.$base.'}/'.$relationship;
            $relationshipUri    = $uri.'/{'.$base.'}/relationships/'.$relationship;

            foreach ($methods as $method) {
                $routeName = implode('.', [$entityType, $relationship, $method]);
                switch($method) {
                    case 'relationshipRelated':
                        $this->get($relatedUri, $controller.'@relationshipRelated')
                            ->name($routeName)
                            ->middleware($middlewares);
                        break;
                    case 'relationshipIndex':
                        $this->get($relationshipUri, $controller.'@relationshipIndex')
                            ->name($routeName)
                            ->middleware($middlewares);
                        break;
                    case 'relationshipStore':
                        $this->post($relationshipUri, $controller.'@relationshipStore')
                            ->name($routeName)
                            ->middleware($middlewares);
                        break;
                    case 'relationshipUpdate':
                        $this->match(['PUT', 'PATCH'], $relationshipUri, $controller.'@relationshipUpdate')
                            ->name($routeName)
                            ->middleware($middlewares);
                        break;
                    case 'relationshipDestroy':
                        $this->delete($relationshipUri, $controller.'@relationshipDestroy')
                            ->name($routeName)
                            ->middleware($middlewares);
                        break;
                    default:
                        throw new UnknownRelationshipVerbException($entityType, $relationship, $method);
                }
            }
        };
	}

	/**
     * Load all routes bound to a model.
     *
     * @param  string  $class
     * @param  array  $options
     * @return void
     */
	protected function jsonApiResource()
	{
        $relationshipDefaults = config('json-api.relationships');

		return function (string $class, array $options = []) use ($relationshipDefaults) {
            if ($routerMethod = $class::getRouterMethod()) {
                $middleware = $options['middleware'] ?? [];

                $as = $options['as'] ?? '';

                $prefix = $class::getRouterPrefix();

                $this->group(compact('middleware', 'as', 'prefix'), function ($router) use ($class, $routerMethod, $options, $relationshipDefaults) {
                    $alias          = $class::getAlias();
                    $controller     = $class::getControllerClass();
                    $routerOptions  = $class::getRouterSettings();

                    $basicRoutes = Arr::only(
                        $routerOptions,
                        ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy']
                    );

                    foreach ($basicRoutes as $method => $middlewares) {
                        if (is_null($middlewares)) {
                            continue;
                        }
                        $router->{$routerMethod}(
                            $alias,
                            $controller,
                            [
                                'only' => $method,
                                'middleware' => $middlewares
                            ]
                        );
                    }

                    $relationshipRoutes = Arr::only(
                        $routerOptions,
                        $relationshipDefaults
                    );

                    foreach ($relationshipRoutes as $relationshipMethod => $relationships) {
                        foreach ($relationships as $relationship => $middlewares) {
                            if (is_null($middlewares)) {
                                continue;
                            }
                            $router->resourceRelationship(
                                $alias,
                                $relationship,
                                $controller,
                                [ $relationshipMethod ],
                                $middlewares
                            );
                        }
                    }
                });
            }
		};
	}
}

<?php

namespace Shizzen\JsonApi\Mixins;

class Arr
{
	protected function wrapWhen()
	{
		return function($predicate, $data) {
			if (is_callable($predicate)) {
                return $predicate($data) ? [ $data ] : $data;
            }
            return $predicate ? [ $data ] : $data;
        };
    }

    protected function wrapUnless()
    {
    	return function ($predicate, $data) {
            if (is_callable($predicate)) {
                return !$predicate($data) ? [ $data ] : $data;
            }
            return !$predicate ? [ $data ] : $data;
    	};
    }
}

<?php

namespace Shizzen\JsonApi\Mixins;

use Illuminate\Contracts\Pagination\{
	Paginator,
	LengthAwarePaginator
};

class Collection
{
	protected function isValid()
	{
		return function (?object $data) {
			if (is_null($data)) {
				return false;
			}
			return $data instanceof static;
		};
	}

	protected function isPaginable()
	{
		return function (?object $data) {
			if (is_null($data)) {
				return false;
			}
			return static::isValid($data) || $data instanceof Paginator || $data instanceof LengthAwarePaginator;
		};
	}

	protected function prefixBy()
	{
		return function (string $prefix) {
			return $this->keyBy(function ($value, $attribute) use ($prefix) {
                return $prefix ? "{$prefix}.{$attribute}" : $attribute;
            });
		};
	}
}

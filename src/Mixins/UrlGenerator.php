<?php

namespace Shizzen\JsonApi\Mixins;

use Illuminate\Support\Str;

class UrlGenerator
{
	protected function jsonApiRelationship()
	{
		return function (string $parent, string $id, string $relationship, string $method, array $additionalParameters = []) {
            $method = ucfirst($method);

			return $this->route(
                config('json-api.prefix')."{$parent}.{$relationship}.relationship{$method}",
                array_merge(
                	$additionalParameters,
                	[ Str::singular($parent) => $id ]
                )
            );
		};
	}
}

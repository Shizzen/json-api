<?php

namespace Shizzen\JsonApi\Mixins;

use Illuminate\Support\{
    Arr,
    Str
};
use Illuminate\Database\{
    Query\Builder,
    Eloquent\ModelNotFoundException,
    Eloquent\Relations\Relation
};

class Route
{
    protected function isJsonApi()
    {
        return function () {
            $prefix = config('json-api.prefix');
            return $this->named($prefix . '*');
        };
    }

    protected function nameFragments()
    {
    return function () {
            $routeName = $this->getName();

            if (!$this->isJsonApi()) {
                return explode('.', $routeName);
            }

            $fragments = explode('.', Str::after($routeName, config('json-api.prefix')));

            switch (count($fragments)) {
                case 2:
                    return array_combine(['entityType', 'method'], $fragments);
                case 3:
                    return array_combine(['entityType', 'relationship', 'method'], $fragments);
                default:
                    return $fragments;
            }
        };
    }

    protected function entityType()
    {
        return function () {
            return Arr::get($this->nameFragments(), 'entityType');
        };
    }

    protected function entityClass()
    {
        return function (bool $build = false) {
            $entityClass = Relation::getMorphedModel($this->entityType());
            return $build
                ? (new $entityClass)->newInstance()
                : $entityClass;
        };
    }

    protected function entityId()
    {
        return function () {
            return $this->parameter(Str::singular($this->entityType()));
        };
    }

    protected function relationship()
    {
        return function () {
            return Arr::get($this->nameFragments(), 'relationship');
        };
    }

    protected function method()
    {
        return function () {
            return Arr::get($this->nameFragments(), 'method');
        };
    }

    protected function entity()
    {
        return function () {
            $entityClass    = $this->entityClass();
            $entityId       = $this->entityId();

            if (is_null($entityId)) {
                throw (new ModelNotFoundException)->setModel($entityClass);
            }
            return (new $entityClass)->query()->findOrFail($entityId);
        };
    }

    protected function isToMany()
    {
        return function () {
            $relationship = $this->relationship();
            return !is_null($relationship) && Str::isPlural($relationship);
        };
    }
}

<?php

namespace Shizzen\JsonApi\Mixins;

use Illuminate\Support\Collection;

class Response
{
    protected function jsonApi()
    {
        return function ($resource, bool $full = true, int $statusCode = 200) {
            $resourceCollectionClass = $resource->getResourceCollectionClass();

            $resource = Collection::isPaginable($resource)
                ? new $resourceCollectionClass($resource, $full)
                : $resourceCollectionClass::makeSingle($resource, $full);
            return $resource->setStatusCode($statusCode);
        };
    }
}

<?php

namespace Shizzen\JsonApi\Mixins;

use Illuminate\Support\{
	Arr,
	Str,
    Collection
};
use Illuminate\Database\Eloquent\Relations\{
    MorphTo,
	BelongsTo,
    BelongsToMany,
	Concerns\InteractsWithPivotTable
};
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Shizzen\JsonApi\Exceptions\BoundKeyNotFoundException;

class Relation
{
    /**
     * Check whether the relation uses a pivot table.
     *
     * @return bool
     */
	protected function usePivot()
	{
		return function () {
			return in_array(
                InteractsWithPivotTable::class,
                class_uses_recursive($this)
            );
		};
	}

	protected function updateRelated()
	{
		$requestMethod = app()->make('request')->method();

		return function ($related, string $httpMethod = null) use ($requestMethod) {
			$httpMethod = $httpMethod ?: $requestMethod;
            if ($this instanceof BelongsTo) {
                if ($httpMethod === 'DELETE') {
                    $this->dissociate();
                }
                else {
                    $this->associate($related);
                }
            }
            elseif ($this->usePivot()) {
                switch ($httpMethod) {
                    case 'POST':
                        $saveMethod = 'syncWithoutDetaching';
                        break;
                    case 'PUT':
                    case 'PATCH':
                        $saveMethod = 'sync';
                        break;
                    case 'DELETE':
                        $saveMethod = 'detach';
                }

                $relationshipData = Arr::wrapWhen(Str::isSingular($name), $related['data']);

                $this->{$saveMethod}(array_column($relationshipData, 'id'));
            }

            return $this;
		};
	}

	protected function postIdentifiers()
	{
		return function (array $identifiers = null) {
			switch (true) {
                case $this instanceof BelongsToMany:
                    $this->syncWithoutDetaching(array_column($identifiers, 'id'));
                    break;
                case $this instanceof BelongsTo:
                    if (! $newRelated = static::parseIdentifiers($identifiers)->first() ) {
                        dd('moche');
                    }
                    $this->associate($newRelated)->save();
                    break;
                default:
                    return response()->json(null, 405);
            }
            return response()->json(null, 204);
		};
	}

	protected function patchIdentifiers()
	{
		return function (array $identifiers = null) {
			switch (true) {
                case $this instanceof BelongsToMany:
                    $this->sync(array_column($identifiers, 'id'));
                    break;
                case $this instanceof BelongsTo:
                    if (! $newRelated = static::parseIdentifiers($identifiers)->first() ) {
                        dd($newRelated, $identifiers);
                    }
                    $this->associate($newRelated)->save();
                    break;
                default:
                    return response()->json(null, 405);
            }
            return response()->json(null, 204);
		};
	}

	protected function deleteIdentifiers()
	{
		return function (array $identifiers = null) {
			switch (true) {
                case $this instanceof BelongsToMany:
                    $this->detach(array_column($identifiers, 'id'));
                    break;
                case $this instanceof BelongsTo:
                    $newRelated = static::parseIdentifiers($identifiers)->first();
                    $oldRelated = $this->getChild()->{$this->getRelationName()};
                    if ($newRelated->is($oldRelated)) {
                        $this->dissociate()->save();
                    }
                    break;
                default:
                    return response()->json(null, 405);
            }
            return response()->json(null, 204);
		};
	}

    protected function parseIdentifier()
    {
        return function ($type, $ids) {
            return is_object($type)
                ? $type->find($ids)
                : static::getMorphedModel($type)::find($ids);
        };
    }

    protected function parseIdentifiers()
    {
        return function (array $identifiers) {
            return Collection::make($identifiers)
                ->mapToGroups(function ($identifier) {
                    return [$identifier['type'] => $identifier['id']];
                })
                ->flatMap(function ($ids, $type) {
                    return static::parseIdentifier($type, $ids);
                });
        };
    }

    /**
     * Get model alias from table.
     *
     * @param  string  $table
     * @return string
     */
    protected function guessType()
    {
        return function (string $table) {
            foreach (static::morphMap() as $alias => $model) {
                if ($table === (new $model)->getTable()) {
                    return $alias;
                }
            }
            return $table;
        };
    }

    /**
     * Get all foreign keys needed to retrieve the relation.
     *
     * @return array
     */
    protected function getNeededForeignKeys()
    {
        return function () {
            $foreignKeys = [];
            if ($this instanceof BelongsTo) {
                $foreignKeys[] = $this->getForeignKeyName();
                if ($this instanceof MorphTo) {
                    $foreignKeys[] = $this->getMorphType();
                }
            }
            return $foreignKeys;
        };
    }

    /**
     * Find the first model matching a bound key.
     *
     * @param  mixed  $key
     * @return mixed|null
     */
    protected function findByBoundKey()
    {
        return function ($key) {
            $relatedModel = $this->getRelated();
            if (! $boundKeyName = $relatedModel->getBoundKeyName()) {
                throw new BoundKeyNotFoundException(get_class($relatedModel));
            }
            return $this->where($boundKeyName, $key)->first();
        };
    }

    protected function fillOrCreate()
    {
        return function (array $filters, array $attributes) {
            try {
                $relatedModel = $this->where($filters)
                    ->firstOrFail()
                    ->fill($attributes);
                $relatedModel->save();
            }
            catch (ModelNotFoundException $e) {
                if ($this instanceof BelongsToMany) {
                    $relatedModel = $this->getRelated()->query()->where($filters)->firstOrFail();
                    $this->attach($relatedModel);
                    return $relatedModel;
                }

                $relatedModel = $this->getRelated()->newInstance($attributes);
                $this->save($relatedModel);
            }
            return $relatedModel;
        };
    }
}

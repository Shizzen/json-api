<?php

namespace Shizzen\JsonApi\Mixins;

class Str
{
    protected function isSingular()
    {
        return function (string $value) {
            return $value === static::singular($value);
        };
    }

    protected function isPlural()
    {
        return function (string $value) {
            return $value === static::plural($value);
        };
    }

    protected function isJsonApiValid()
    {
        $patterns = [
            '|^[[:alnum:]][[:alnum:]_-]*[[:alnum:]]$|',
            '|^[[:alnum:]]$|'
        ];
        return function (string $value, string $separator = null) use ($patterns) {
            $value = $separator ? explode($separator, $value) : [ $value ];
            foreach ($value as $valuePiece) {
                foreach ($patterns as $pattern) {
                    if (preg_match($pattern, $valuePiece)) {
                        return true;
                    }
                }
            }
            return false;
        };
    }
}

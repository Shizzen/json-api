<?php

namespace Shizzen\JsonApi\Commands;

use Illuminate\Console\GeneratorCommand;
use Shizzen\JsonApi\Traits\StubModelParser;
use Symfony\Component\Console\Input\InputOption;

class ExceptionMakeCommand extends GeneratorCommand
{
    use StubModelParser;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'json-api:make-exception';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new exception able to render a JSON:API error';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Exception';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return $this->option('model')
            ? __DIR__.'/../../stubs/exception.stub'
            : __DIR__.'/../../stubs/exception.plain.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        $exceptionNamespace = $rootNamespace.'\Exceptions';
        if ($model = $this->option('model')) {
            return $exceptionNamespace.'\\'.$model;
        }
        return $exceptionNamespace;
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildClass($name)
    {
        $logChannel = $this->option('channel');

        $replace = [
            'DummyLogChannel'   => is_null($logChannel) ? $logChannel : json_encode($logChannel),
            'DummyLogLevel'     => json_encode($this->option('level') ?: 'info'),
            'DummyCode'         => $this->option('code') ?: 400,
            'DummyDescription'  => json_encode($this->option('description') ?: '')
        ];

        if ($this->option('model')) {
            $replace = $this->buildModelReplacements($replace);
        }

        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array_merge(
            parent::getOptions(),
            [
                ['model', 'm', InputOption::VALUE_OPTIONAL, 'Bind the exception to a given model.'],
                ['channel', null, InputOption::VALUE_OPTIONAL, 'The log channel used by the exception.'],
                ['level', 'l', InputOption::VALUE_OPTIONAL, 'The log level used by the exception.'],
                ['code', 'c', InputOption::VALUE_OPTIONAL, 'The HTTP response code used to render the exception.'],
                ['description', 'd', InputOption::VALUE_OPTIONAL, 'The description used to render the exception.'],
            ]
        );
    }
}

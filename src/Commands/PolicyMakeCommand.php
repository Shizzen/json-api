<?php

namespace Shizzen\JsonApi\Commands;

use Illuminate\Foundation\Console\PolicyMakeCommand as BasePolicyMakeCommand;

class PolicyMakeCommand extends BasePolicyMakeCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'json-api:make-policy';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return $this->option('model')
            ? __DIR__.'/../../stubs/policy.stub'
            : __DIR__.'/../../stubs/policy.plain.stub';
    }
}

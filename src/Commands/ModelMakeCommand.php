<?php

namespace Shizzen\JsonApi\Commands;

use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Foundation\Console\ModelMakeCommand as BaseModelMakeCommand;

class ModelMakeCommand extends BaseModelMakeCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'json-api:make-model';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (parent::handle() === false) {
            return false;
        }

        $this->info('Do not forget to add this model into "models" array of config/json-api.php !');
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildClass($name)
    {
        $apiType = $this->option('type') ?? Str::plural(Str::kebab(class_basename($name)));

        return str_replace(
            [
                'DummyModelType',
                'DummyRouterMethod'
            ],
            [
                json_encode($apiType),
                json_encode($this->option('router-method'))
            ],
            parent::buildClass($name)
        );
    }

    /**
     * Create a controller for the model.
     *
     * @return void
     */
    protected function createController()
    {
        $controller = Str::studly(class_basename($this->getNameInput()));

        $this->call('json-api:make-controller', [
            'name' => "{$controller}Controller",
            '--model' => $this->getNameInput()
        ]);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return $this->option('pivot')
            ? __DIR__.'/../../stubs/pivot.model.stub'
            : __DIR__.'/../../stubs/model.stub';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array_merge(
            parent::getOptions(),
            [
                ['type', null, InputOption::VALUE_OPTIONAL, 'API type'],
                ['router-method', null, InputOption::VALUE_OPTIONAL, 'Router method used to generate model routes'],
            ]
        );
    }
}

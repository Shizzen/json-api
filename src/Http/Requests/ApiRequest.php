<?php

namespace Shizzen\JsonApi\Http\Requests;

use Illuminate\Support\{
    Str,
    Collection
};
use Illuminate\Database\Eloquent\{
    Relations\Relation,
    ModelNotFoundException,
};
use Illuminate\Foundation\{
    Http\FormRequest,
    Auth\Access\AuthorizesRequests
};
use Shizzen\JsonApi\{
    Rules\ValidModel,
    Rules\ValidRelationship,
    Exceptions\JsonApiException,
    Contracts\ApiRequest as ApiRequestContract
};
use Illuminate\Validation\Rule;

class ApiRequest extends FormRequest implements ApiRequestContract
{
    use AuthorizesRequests {
        authorize as checkPolicy;
        authorizeForUser as checkPolicyForUser;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!$currentRoute = $this->route()) {
            throw new JsonApiException(
                500,
                [],
                [],
                [],
                [],
                'Something probably went wrong about your routes, you should check them and also relationship verbs.'
            );
        }
        $method         = $currentRoute->getActionMethod();
        $modelClass     = $currentRoute->entityClass();

        if (! method_exists(policy($modelClass), $method)) {
            return true;
        }

        try {
            $model = $currentRoute->entity();
        }
        catch (ModelNotFoundException $e) {
            return $this->checkPolicyForUser(
                $this->user(), $method, $modelClass
            );
        }

        return $this->checkPolicyForUser(
            $this->user(), $method, $model
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (true) {
            case $this->mustHaveEmptyBody():
                return [];
            case $this->mustHaveResourceObject():
                return $this->resourceObjectRules();
            case $this->mustHaveResourceIdentifier():
                return $this->resourceIdentifierRules();
            default:
                abort(400, 'Request not conform to JSON:API spec');
        }
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        return optional($this->validator)->validated();
    }

    /**
     * Check whether the request must have empty body.
     *
     * @return bool
     */
    public function mustHaveEmptyBody()
    {
        $httpMethod = $this->method();
        if ($httpMethod === 'DELETE') {
            return is_null($this->relationship());
        }
        return $httpMethod === 'GET';
    }

    /**
     * Check whether the request must have a resource object.
     *
     * @return bool
     */
    public function mustHaveResourceObject()
    {
        return is_null($this->relationship()) && !$this->mustHaveEmptyBody();
    }

    /**
     * Check whether the request must have at least one resource identifier.
     *
     * @return bool
     */
    public function mustHaveResourceIdentifier()
    {
        return !is_null($this->relationship()) && !$this->mustHaveEmptyBody();
    }

    /**
     * Return resource identifier rules.
     *
     * @return array
     */
    protected function resourceIdentifierRules()
    {
        $identifierRules = Collection::make([
            'data.type' => new ValidModel,
            'data.id'   => 'required|string',
        ]);
        if ($this->route()->isToMany()) {
            $identifierRules = $identifierRules->keyBy(function ($rules, $attribute) {
                    return str_replace('data.', 'data.*.', $attribute);
                })
                ->put('data.*', 'filled|array');
        }
        return $identifierRules->put('data', 'present|array')->all();
    }

    /**
     * Return resource object rules.
     *
     * @return array
     */
    protected function resourceObjectRules()
    {
        return [
            'data'                  => 'required|array',
            'data.type'             => new ValidModel,
            'data.id'               => [ Rule::requiredIf(!$this->isMethod('post')), 'string' ],
            'data.attributes'       => 'array',
            'data.relationships'    => 'filled|array',
            'data.relationships.*'  => new ValidRelationship
        ];
    }

    /**
     * Get filters from query string.
     *
     * @return array
     */
    public function filters()
    {
        return $this->query('filter', []);
    }

    /**
     * Get included relationships from query string.
     *
     * @return array
     */
    public function included()
    {
        return array_filter(explode(',', $this->query('include', '')), 'strlen');
    }

    /**
     * Get sorts from query string.
     *
     * @return array
     */
    public function sorts()
    {
        return array_filter(explode(',', $this->query('sort', '')), 'strlen');
    }

    /**
     * Get page from query string.
     *
     * @return int|null
     */
    public function page()
    {
        return (int) $this->query('page');
    }

    /**
     * Get items number per page from query string.
     *
     * @return int|null
     */
    public function perPage()
    {
        return (int) $this->query('perPage', config('json-api.pagination.perPage'));
    }

    /**
     * Get fields from query string.
     *
     * @return array
     */
    public function fields()
    {
        return array_map(
            function ($fields) { return explode(',', $fields); },
            $this->query('fields', [])
        );
    }

    /**
     * Get attributes from request body.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->input('data.attributes', []);
    }

    /**
     * Get relationships from request body.
     *
     * @return array
     */
    public function relationships()
    {
        return $this->input('data.relationships', []);
    }

    /**
     * Get the Eloquent model bound to this request.
     *
     * @param  bool  $build
     * @return \Illuminate\Database\Eloquent\Model|string
     */
    public function model(bool $build = true)
    {
        return $this->route()->entityClass($build);
    }

    /**
     * Get the relationship present in current route.
     *
     * @return string|null
     */
    public function relationship()
    {
        return $this->route()->relationship();
    }

    /**
     * Parse models present in request relationships.
     *
     * @return \Illuminate\Support\Collection
     */
    public function relatedModels()
    {
        $modelFinder = function ($data) {
            return Relation::parseIdentifier($data['type'], $data['id']);
        };
        return Collection::make($this->relationships())
            ->map(function ($relationshipObject, $name) use ($modelFinder) {
                $identifiers = $relationshipObject['data'];
                return ($identifiers['type'] ?? false)
                    ? $modelFinder($identifiers)
                    : array_map($modelFinder, $identifiers);
            });
    }
}

<?php

namespace Shizzen\JsonApi\Http\Middleware;

use Illuminate\Support\{
    Str,
    Collection
};
use Shizzen\JsonApi\{
    Contracts\ApiRequest,
    Exceptions\ExceptionAggregator,
    Exceptions\HeaderParameterException,
    Exceptions\UnparsableIncludeException,
    Exceptions\NotAllowedQueryParameterException
};
use Closure;
use Illuminate\Contracts\Foundation\Application;

class VerifyJsonApi
{
    protected $headersMapping = [
        'Accept'        => 406,
        'Content-Type'  => 415,
    ];

    /**
     * The application instance.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * The application instance.
     *
     * @var array
     */
    protected $queryParameters;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  array  $queryParameters
     */
    public function __construct(Application $app, array $queryParameters)
    {
        $this->app = $app;
        $this->queryParameters = $queryParameters;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @throws \Shizzen\JsonApi\Exceptions\ExceptionAggregator
     */
    public function handle($request, Closure $next)
    {
        $jsonApiRequest = $this->app[ApiRequest::class];

        $this->getIncludeErrors($jsonApiRequest)
            ->merge($this->getNotAllowedParameterErrors($jsonApiRequest))
            ->merge($this->getHeaderParameterErrors($jsonApiRequest))
            ->whenNotEmpty(function ($errors) { throw new ExceptionAggregator($errors); });

        return $next($jsonApiRequest);
    }

    /**
     * Detect misformatted included relationships.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @return \Illuminate\Support\Collection
     */
    protected function getIncludeErrors(ApiRequest $request)
    {
        return Collection::make($request->included())
            ->filter(function ($relationship) {
                foreach (explode('.', $relationship) as $relationshipFragment) {
                    if (! Str::isJsonApiValid($relationshipFragment)) {
                        return true;
                    }
                }
                return false;
            })
            ->pipe(function ($wrongRelationships) {
                return UnparsableIncludeException::spread($wrongRelationships);
            });
    }

    /**
     * Detect not allowed parameters in query string.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @return \Illuminate\Support\Collection
     */
    protected function getNotAllowedParameterErrors(ApiRequest $request)
    {
        $notAllowedParameters = array_diff(
            array_keys($request->query()), $this->queryParameters
        );
        return NotAllowedQueryParameterException::spread($notAllowedParameters);
    }

    /**
     * Detect headers which got wrong parameters.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @return \Illuminate\Support\Collection
     */
    protected function getHeaderParameterErrors(ApiRequest $request)
    {
        return Collection::make($this->headersMapping)
            ->filter(function ($status, $header) use ($request) {
                return Str::contains($request->header($header, ''), ';');
            })
            ->map(function ($status, $header) {
                return new HeaderParameterException($header, $status);
            })
            ->values();
    }
}

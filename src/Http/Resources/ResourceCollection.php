<?php

namespace Shizzen\JsonApi\Http\Resources;

use Illuminate\Http\Resources\{
    MissingValue,
    Json\ResourceCollection as BaseResourceCollection
};
use ReflectionClass;
use Shizzen\JsonApi\Traits\JsonApiWrapper;
use Illuminate\Pagination\AbstractPaginator;

class ResourceCollection extends BaseResourceCollection
{
    use JsonApiWrapper;

    /**
     * True if resource fetches all model data, only identifiers otherwise.
     *
     * @var bool
     */
    protected $full;

    /**
     * Instantiate a new resource collection.
     *
     * @param  mixed  $resource
     * @param  bool  $full
     */
    public function __construct($resource, bool $full = true)
    {
        $this->full = $full;

        parent::__construct($resource);
    }

    /**
     * Get included data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Support\Collection
     */
    public function getIncluded($request)
    {
        return $this->collection
            ->flatMap->getIncluded($request)
            ->unique->getApiIdentifiers()
            ->values();
    }

    /**
     * Instantiate the single resource class bound to this collection.
     *
     * @param  mixed|null  $resource
     * @param  bool  $full
     * @param  string  $statusCode
     * @return mixed
     */
    public static function makeSingle($resource, bool $full, string $statusCode = '200')
    {
        $singleClass = (new ReflectionClass(static::class))
            ->newInstanceWithoutConstructor()
            ->collects();
        return new $singleClass($resource, $full, $statusCode);
    }

    /**
     * Get resource meta data.
     *
     * @return array
     */
    public function getApiMeta()
    {
        return [
            'count' => $this->collection->count()
        ];
    }

    /**
     * Map the given collection resource into its individual resources.
     *
     * @param  mixed  $resource
     * @return mixed
     */
    protected function collectResource($resource)
    {
        if ($resource instanceof MissingValue) {
            return $resource;
        }

        $collects = $this->collects();

        $this->collection = $collects && ! $resource->first() instanceof $collects
            ? $resource->map(function ($model) use ($collects) {
                return new $collects($model, $this->full);
            })
            : $resource->toBase();

        return $resource instanceof AbstractPaginator
            ? $resource->setCollection($this->collection)
            : $this->collection;
    }
}

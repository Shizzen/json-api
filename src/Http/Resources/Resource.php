<?php

namespace Shizzen\JsonApi\Http\Resources;

use Shizzen\JsonApi\{
    Traits\JsonApiWrapper,
    Contracts\JsonApiModel
};
use Illuminate\Support\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class Resource extends JsonResource
{
    use JsonApiWrapper;

    /**
     * True if resource fetches all model data, only identifiers otherwise.
     *
     * @var bool
     */
    protected $full;

    /**
     * Instantiate a new resource.
     *
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $resource
     * @param  bool  $full
     */
    public function __construct(JsonApiModel $resource, bool $full)
    {
        $this->full = $full;

        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @return array|null
     */
    public function toArray($request)
    {
        if (is_null($this->resource)) {
            return null;
        }

        return array_merge(
            $this->resource->getApiIdentifiers(),
            [
                $this->mergeWhen($this->full, function () use ($request) {
                    $relationships = $this->formatRelations($request);
                    return [
                        'attributes' => $this->resource->getApiAttributes()->toArray(),
                        'relationships' => $this->when($relationships->isNotEmpty(), $relationships),
                        'links' => $this->resource->getApiLinks()
                    ];
                })
            ]
        );
    }

    /**
     * Format model relations.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @return \Illuminate\Support\Collection
     */
    public function formatRelations($request)
    {
        return $this->resource
            ->getApiRelations()
            ->map(function ($relationship, $name) {
                return [
                    'links' => $this->resource->getApiLinks($name),
                    'data'  => response()->jsonApi($relationship, false),
                    'meta'  => $this->when($relationship, function () use ($relationship) { return $relationship->getApiMeta(); })
                ];
            });
    }

    /**
     * Get included data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Support\Collection|null
     */
    public function getIncluded($request)
    {
        if (is_null($this->resource)) {
            return null;
        }
        return $this->resource
            ->getRelatedModels()
            ->map(function ($model) {
                return new static($model, true);
            })
            ->whenEmpty(function () {
                return null;
            });
    }

    /**
     * Get resource meta data.
     *
     * @return array
     */
    public function getApiMeta()
    {
        return $this->resource->getApiMeta();
    }
}

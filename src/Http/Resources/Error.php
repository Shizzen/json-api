<?php

namespace Shizzen\JsonApi\Http\Resources;

use Shizzen\JsonApi\{
    Traits\JsonApiWrapper,
    Exceptions\JsonApiException
};
use Illuminate\Database\Eloquent\{
    ModelNotFoundException,
    RelationNotFoundException
};
use Throwable;
use Illuminate\Support\Str;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class Error extends JsonResource
{
    use JsonApiWrapper;

    /**
     * A translator instance (lazy-getter).
     *
     * @var \Illuminate\Contracts\Translation\Translator
     */
    protected $translator;

    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'error';

    /**
     * The "data" wrapper that should be applied.
     *
     * @var array
     */
    public $errorLinks = [];

    /**
     * The "data" wrapper that should be applied.
     *
     * @var array
     */
    public $errorSource = [];

    /**
     * The "data" wrapper that should be applied.
     *
     * @var array
     */
    public $errorMeta = [];

    /**
     * Instantiate a new error.
     *
     * @param  \Throwable  $error
     */
    public function __construct(Throwable $error)
    {
        parent::__construct($error);

        if ($error instanceof HttpExceptionInterface) {
            $this->setStatusCode($error->getStatusCode());
            $this->setHeaders($error->getHeaders());
            if ($error instanceof JsonApiException) {
                $this->errorLinks = $error->links;
                $this->errorSource = $error->source;
                $this->errorMeta = $error->meta;
            }
        }
        elseif ($error instanceof RelationNotFoundException) {
            $this->setStatusCode(404);
            $this->errorSource = [
                'parameter' => 'include'
            ];
            $this->errorMeta = [
                'type'      => $error->model->getAlias(),
                'relation'  => $error->relation,
            ];
        }
        elseif ($error instanceof ModelNotFoundException) {
            $this->setStatusCode(404);
            $this->errorMeta = [
                'type'  => $error->getModel()::getAlias(),
                'ids'   => $error->getIds(),
            ];
        }

        $this->errorDetail ??= $error->getMessage();

        $this->errorTitle ??= Str::before(class_basename($error), 'Exception');
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status'    => (string) $this->getStatusCode(),
            'code'      => (string) $this->getCode(),
            'id'        => $this->getId(),
            'title'     => $this->getTitle(),
            'detail'    => $this->getMessage(),
            'source'    => $this->when($this->errorSource, $this->errorSource),
            'meta'      => $this->when($this->errorMeta, $this->errorMeta),
            'links'     => $this->when($this->errorLinks, $this->errorLinks),
        ];
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response  $response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $response
            ->setStatusCode($this->getStatusCode())
            ->header('Content-Type', config('json-api.media.name'));
    }

    /**
     * Prepare the exception message by translating and templating.
     *
     * @return string
     */
    public function getMessage()
    {
        $translator = $this->getTranslator();
        $rawMessage = $this->resource->getMessage() ?: JsonApiException::getDefaultTitle($this->resource);
        $translationKey = 'json-api::errors.' . $rawMessage . '_message';

        if (! $translator->has($translationKey)) {
            return $rawMessage;
        }

        $translationData = array_merge($this->errorLinks, $this->errorMeta);

        return isset($translationData['count'])
            ? $translator->choice($translationKey, $translationData['count'], $translationData)
            : $translator->get($translationKey, $translationData);
    }

    /**
     * Get a short, human-readable summary of the problem.
     *
     * @return string
     */
    public function getTitle()
    {
        $translator = $this->getTranslator();
        $rawTitle = $this->resource instanceof JsonApiException
            ? $this->resource->getTitle()
            : JsonApiException::getDefaultTitle($this->resource);
        $translationKey = 'json-api::errors.' . $rawTitle;

        return $translator->has($translationKey)
            ? $translator->get($translationKey)
            : $rawTitle;
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return $this->resource instanceof JsonApiException
            ? $this->resource->getId()
            : sprintf('%s:%s', JsonApiException::getDefaultTitle($this->resource), $this->resource->getMessage());
    }

    /**
     * Get a translator instance.
     *
     * @return \Illuminate\Contracts\Translation\Translator
     */
    public function getTranslator()
    {
        return $this->translator ??= $this->getContainer()['translator'];
    }
}

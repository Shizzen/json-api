<?php

namespace Shizzen\JsonApi\Http\Resources;

use Shizzen\JsonApi\{
    Traits\JsonApiWrapper,
    Exceptions\ExceptionAggregator
};
use Illuminate\Http\Resources\Json\ResourceCollection;

class ErrorCollection extends ResourceCollection
{
    use JsonApiWrapper;

    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'errors';

    /**
     * Instantiate a new resource collection.
     *
     * @param  mixed  $resource
     */
    public function __construct($resource)
    {
        parent::__construct(
            $resource instanceof ExceptionAggregator ? $resource->collects() : $resource
        );
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response  $response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $allStatusCodes = $this->collection->map->getStatusCode();

        $responseStatus = $allStatusCodes->unique()->count() == 1
            ? $allStatusCodes->first()
            : intdiv($allStatusCodes->min(), 100) * 100;

        $response
            ->setStatusCode($responseStatus)
            ->header('Content-Type', config('json-api.media.name'));
    }
}

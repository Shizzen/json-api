<?php

namespace Shizzen\JsonApi\Http\Controllers;

use Illuminate\Foundation\{
    Bus\DispatchesJobs,
    Validation\ValidatesRequests,
    Auth\Access\AuthorizesRequests
};
use Illuminate\Support\Arr;
use Illuminate\Routing\Controller;
use Shizzen\JsonApi\Contracts\ApiRequest;
use Shizzen\JsonApi\Contracts\JsonApiModel;

class ResourceController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Display a listing of the resource.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function index(ApiRequest $request, JsonApiModel $model)
    {
        $models = $model->query()
            ->with($request->included())
            ->mapFilters($request->filters())
            ->mapSorts($request->sorts())
            ->when(
                $request->page(),
                function ($builder) use ($request) {
                    return $builder->paginate($request->perPage());
                },
                function ($builder) {
                    return $builder->get();
                }
            );
        return response()->jsonApi($models);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function store(ApiRequest $request, JsonApiModel $model)
    {
        $model = $request->model()
            ->fill($request->attributes())
            ->saveRelations($request->relatedModels());

        $model->save();

        $model->loadMissing($request->included());
        return response()->jsonApi($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function show(ApiRequest $request, JsonApiModel $model)
    {
        $id = $request->route()->entityId();

        $model = $request
            ->model()
            ->query()
            ->with($request->included())
            ->findOrFail($id);
        return response()->jsonApi($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function update(ApiRequest $request, JsonApiModel $model)
    {
        $attributes = $request->attributes();

        $id = $request->route()->entityId();

        $model = $request
            ->model()
            ->query()
            ->with($request->included())
            ->findOrFail($id)
            ->fill($attributes)
            ->saveRelations($request->relatedModels());

        $model->saveOrFail();

        return $model->wasChangedAsWished($attributes)
            ? response()->json(null, 204)
            : response()->jsonApi($model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApiRequest $request, JsonApiModel $model)
    {
        $id = $request->route()->entityId();

        $request->model()
            ->query()
            ->findOrFail($id)
            ->delete();
        return response()->json(null, 204);
    }

    /**
     * Display the related resources from a relationship.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function relationshipRelated(ApiRequest $request, JsonApiModel $model)
    {
        $id = $request->route()->entityId();

        $related = $request
            ->model()
            ->query()
            ->with($request->included())
            ->findOrFail($id)
            ->{$request->relationship()};
        return response()->jsonApi($related);
    }

    /**
     * Display a relationship.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function relationshipIndex(ApiRequest $request, JsonApiModel $model)
    {
        $id = $request->route()->entityId();

        $related = $request
            ->model()
            ->query()
            ->findOrFail($id)
            ->{$request->relationship()};
        return response()->jsonApi($related, false);
    }

    /**
     * Store a relationship.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function relationshipStore(ApiRequest $request, JsonApiModel $model)
    {
        $id = $request->route()->entityId();

        $relation = $request
            ->model()
            ->query()
            ->findOrFail($id)
            ->{$request->relationship()}();

        $identifiers = Arr::wrapWhen(
            !$request->route()->isToMany(),
            $request->validated()['data']
        );

        return $relation->postIdentifiers($identifiers);
    }

    /**
     * Update a relationship.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function relationshipUpdate(ApiRequest $request, JsonApiModel $model)
    {
        $id = $request->route()->entityId();

        $relation = $request
            ->model()
            ->query()
            ->findOrFail($id)
            ->{$request->relationship()}();

        $identifiers = Arr::wrapWhen(
            !$request->route()->isToMany(),
            $request->validated()['data']
        );

        return $relation->patchIdentifiers($identifiers);
    }

    /**
     * Destroy a relationship.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @return \Illuminate\Http\Response
     */
    public function relationshipDestroy(ApiRequest $request, JsonApiModel $model)
    {
        $id = $request->route()->entityId();

        $relation = $request
            ->model()
            ->query()
            ->findOrFail($id)
            ->{$request->relationship()}();

        $identifiers = Arr::wrapWhen(
            !$request->route()->isToMany(),
            $request->validated()['data']
        );

        return $relation->deleteIdentifiers($identifiers);
    }
}

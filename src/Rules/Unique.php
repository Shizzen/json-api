<?php

namespace Shizzen\JsonApi\Rules;

use Illuminate\Validation\Rules\Unique as BaseUnique;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Unique extends BaseUnique
{
    /**
     * Create a new rule instance.
     *
     * @param  string  $table
     * @param  string  $column
     */
    public function __construct($table, $column = 'NULL')
    {
        parent::__construct($table, $column);

        try {
            $model = app('request')->route()->entity();
            $this->ignore($model, $model->getKeyName());
        }
        catch (ModelNotFoundException $e) {}
    }
}

<?php

namespace Shizzen\JsonApi\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidIdentifier implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return is_string($value['id']) && (new ValidModel)->passes($attribute, $value['type']);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid identifier.';
    }
}

<?php

namespace Shizzen\JsonApi\Rules;

use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Validation\Rule;

class ValidRelationship implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $identifierRule = new ValidIdentifier;
        if (Str::isSingular($attribute)) {
            return $identifierRule->passes($attribute, $value['data']);
        }
        return Collection::make($value['data'])
            ->every(function ($identifier) use ($attribute, $identifierRule) {
                return $identifierRule->passes($attribute, $identifier);
            });
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid relationship.';
    }
}

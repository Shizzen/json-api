<?php

namespace Shizzen\JsonApi\Traits;

use Illuminate\Support\Collection;
use Illuminate\Contracts\Foundation\Application;
use Shizzen\JsonApi\Http\Resources\ErrorCollection;

trait ReportableAndRenderable
{
    /**
     * The errors collection which can be reported or rendered.
     *
     * @var \Shizzen\JsonApi\Http\Resources\ErrorCollection
     */
    protected $errors;

    /**
     * The log channel used to report this exception.
     *
     * @var string|null
     */
    protected $logChannel = null;

    /**
     * The log level used to report this exception.
     *
     * @var string
     */
    protected $logLevel = 'info';

    /**
     * The JSON options used to encode.
     *
     * @var int
     */
    protected $jsonOptions = JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE;

	/**
     * Report the exception.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function report(Application $app)
    {
        if ($logChannel = $this->logChannel) {
            $app['log']->channel($logChannel)
                ->{$this->logLevel}(
                    $this->toJson($this->jsonOptions)
                );
        }
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Shizzen\JsonApi\Http\Resources\ErrorCollection
     */
    public function render($request)
    {
        return $this->getErrors();
    }

    /**
     * Get errors collection which can be reported or rendered.
     *
     * @return \Shizzen\JsonApi\Http\Resources\ErrorCollection
     */
    public function getErrors()
    {
        return $this->errors ??= new ErrorCollection($this->collects());
    }

    /**
     * Get a collection wrapping exception data.
     *
     * @return \Illuminate\Support\Collection
     */
    public function collects()
    {
        return Collection::wrap($this);
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return $this->getErrors()->toJson($options);
    }

    /**
     * Convert the object to its string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toJson($this->jsonOptions);
    }
}

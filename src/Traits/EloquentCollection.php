<?php

namespace Shizzen\JsonApi\Traits;

use Shizzen\JsonApi\Exceptions\BoundKeyNotFoundException;

trait EloquentCollection
{
    /**
     * Count by model type.
     *
     * @return array
     */
    public function groupCount()
    {
        $total = $this->count();

        return $this
            ->groupBy->getAlias()
            ->map(function ($models) { return count($models); })
            ->put('total', $total);
    }

    /**
     * Get meta data included into API resources.
     *
     * @return array
     */
    public function getApiMeta()
    {
        return [
            'count' => $this->groupCount()
        ];
    }

    public function getRelatedModels()
    {
        return $this->map->getRelatedModels();
    }

    /**
     * Find the first model matching a bound key.
     *
     * @param  mixed  $key
     * @return mixed|null
     */
    public function findByBoundKey($key)
    {
        if ($this->isEmpty()) {
            return null;
        }
        $firstModel = $this->first();
        if (! $boundKeyName = $firstModel->getBoundKeyName()) {
            throw new BoundKeyNotFoundException(get_class($firstModel));
        }
        return $this->firstWhere($boundKeyName, $key);
    }

    /**
     * The resource collection class used to render models.
     *
     * @return string
     */
    public function getResourceCollectionClass()
    {
        if ($queuableClass = $this->getQueueableClass()) {
            return $queuableClass::getResourceCollectionClass();
        }
        return config('json-api.classes.resource-collection');
    }
}

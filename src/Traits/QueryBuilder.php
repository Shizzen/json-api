<?php

namespace Shizzen\JsonApi\Traits;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

trait QueryBuilder
{
    /**
     * The relationships that should be eager loaded.
     *
     * @var \Illuminate\Database\Eloquent\Builder|null
     */
    protected $eloquentBuilder = null;

    /**
     * Execute the query as a "select" statement.
     *
     * @param  array|string  $columns
     * @return \Illuminate\Support\Collection
     */
    public function get($columns = ['*'])
    {
        $eloquentBuilder = $this->getEloquentBuilder();
        if ($columns === ['*'] && $eloquentBuilder) {
            $model      = $eloquentBuilder->getModel();
            $retrieved  = $model->getRetrievedFields();
            if ($retrieved !== ['*']) {
                $columns = $eloquentBuilder->getNeededForeignKeys()
                    ->merge($retrieved)
                    ->prepend($model->getKeyName())
                    ->all();
            }
        }
        return parent::get($columns);
    }

    /**
     * Get the relationships being eagerly loaded.
     *
     * @return \Illuminate\Database\Eloquent\Builder|null
     */
    public function getEloquentBuilder()
    {
        return $this->eloquentBuilder;
    }

    /**
     * Set the relationships being eagerly loaded.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return $this
     */
    public function setEloquentBuilder(EloquentBuilder $builder)
    {
        $this->eloquentBuilder = $builder;

        return $this;
    }
}

<?php

namespace Shizzen\JsonApi\Traits;

use Illuminate\Support\Collection;

trait EloquentBuilder
{
	/**
     * Get all foreign keys needed by relationships being eagerly loaded.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getNeededForeignKeys()
    {
        return Collection::make($this->getEagerLoads())
            ->flatMap(function ($callback, $relationName) {
                return $this
                    ->getRelation($relationName)
                    ->getNeededForeignKeys();
            })
            ->unique();
    }
}
<?php

namespace Shizzen\JsonApi\Traits;

use Illuminate\Support\{
    Str,
    Collection
};
use Shizzen\JsonApi\{
    Http\Resources\ErrorCollection,
    Exceptions\JsonApiException,
    Exceptions\ExceptionAggregator,
    Exceptions\AttributeNotFoundException,
    Exceptions\ValidationException as ApiValidationException
};
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Validation\ValidationException as LaravelValidationException;

trait ExceptionHandler
{
    /**
     * Prepare exception for rendering.
     *
     * @param  \Exception  $e
     * @return \Exception
     */
    protected function prepareException(Exception $e)
    {
        if ($e instanceof LaravelValidationException) {
            $e = Collection::make($e->errors())
                ->flatMap(function ($errorBag, $path) {
                    return ApiValidationException::spread($errorBag, $path);
                })
                ->pipe(function ($errors) {
                    return new ExceptionAggregator($errors);
                });
        } elseif ($e instanceof QueryException) {
            $query = $e->getSql();
            switch ($e->getCode()) {
                case '42S22':
                    preg_match('~(?<= from `)\w+?(?=`)~', $query, $tableMatch);
                    preg_match_all('~(?<=`)\w+?(?=`)~', Str::before($query, ' from '), $attributesMatch);
                    $tableAttributes = $this->container->make('db')
                        ->connection()
                        ->getSchemaBuilder()
                        ->getColumnListing($tableMatch[0]);

                    $fromHttpQuery = $this->container->make('request')->route()->isJsonApi();

                    $attributesExceptions = AttributeNotFoundException::spread(
                        array_diff($attributesMatch[0], $tableAttributes),
                        $tableMatch[0],
                        $fromHttpQuery,
                        $e
                    );
                    $e = new ExceptionAggregator($attributesExceptions);
                    break;
            }
        } elseif ($e instanceof AuthorizationException) {
            $e = new AccessDeniedHttpException($e->getMessage(), $e);
        } elseif ($e instanceof TokenMismatchException) {
            $e = new HttpException(419, $e->getMessage(), $e);
        }

        return $e;
    }

    /**
     * Convert a validation exception into a JSON response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Validation\ValidationException  $exception
     * @return \Shizzen\JsonApi\Http\Resources\ErrorCollection
     */
    protected function invalidJson($request, LaravelValidationException $exception)
    {
        return Collection::make($exception->errors())
            ->flatMap(function ($errorBag, $path) use ($exception) {
                return ApiValidationException::spread($errorBag, $path, 0, $exception);
            })
            ->pipe(function ($exceptions) {
                return new ErrorCollection($exceptions);
            });
    }

    /**
     * Prepare a JSON response for the given exception.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception $e
     * @return \Shizzen\JsonApi\Http\Resources\ErrorCollection
     */
    protected function prepareJsonResponse($request, Exception $e)
    {
        return $e instanceof ExceptionAggregator
            ? new ErrorCollection($e)
            : new ErrorCollection(Collection::wrap($e));
    }

    /**
     * Determine if the exception is in the "do not report" list.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function shouldntReport(Exception $e)
    {
        return !($e instanceof JsonApiException) && parent::shouldntReport($e);
    }
}

<?php

namespace Shizzen\JsonApi\Traits;

use Illuminate\Support\Collection;

trait Spreadable
{
    /**
     * Create as many instances as elements with optional elements.
     *
     * @param  Illuminate\Support\Collection|array  $elements
     * @param  mixed  ...$constructorArgs
     * @return \Illuminate\Support\Collection
     */
    public static function spread($elements, ...$constructorArgs)
    {
        return Collection::wrap($elements)
            ->map(function ($parameter) use ($constructorArgs) {
                return new static($parameter, ...$constructorArgs);
            });
    }
}

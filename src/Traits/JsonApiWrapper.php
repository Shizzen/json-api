<?php

namespace Shizzen\JsonApi\Traits;

use Illuminate\Support\{
    Str,
    Collection
};
use Throwable;
use JsonSerializable;
use Shizzen\JsonApi\Contracts\ApiRequest;
use Illuminate\Contracts\Support\Arrayable;

trait JsonApiWrapper
{
    /**
     * Container instance.
     *
     * @var \Illuminate\Contracts\Container\Container
     */
    protected $container;

    /**
     * HTTP status code.
     *
     * @var string
     */
    protected $statusCode;

    /**
     * HTTP headers.
     *
     * @var array
     */
    protected $headers = [];

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        $meta = array_merge(
            config('json-api.meta', []),
            $this->getApiMeta()
        );

        $included = $this->getIncluded($request);

        return Collection::make([
            'jsonapi' => [
                'version' => config('json-api.media.version', '1.0')
            ],
            'links' => $this->topLinks($request),
        ])
        ->when($included, function ($with, $included) {
            return $with->merge(compact('included'));
        })
        ->when($meta, function ($with, $meta) {
            return $with->merge(compact('meta'));
        })
        ->all();
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response  $response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $response->header('Content-Type', config('json-api.media.name'));

        if (! $request->isMethodCacheable() && $this->resource->wasRecentlyCreated ?? false) {
            $response->setStatusCode(201)
                ->header('Location', $this->resource->getApiLinks()['self']);
        }
        elseif ($this->statusCode ?? false) {
            $response->setStatusCode($this->statusCode);
        }
    }

    /**
     * Get links to be included at top level.
     *
     * @param  \Shizzen\JsonApi\Contracts\ApiRequest  $request
     * @return array
     */
    public function topLinks($request)
    {
        $self           = $request->fullUrl();
        $links          = compact('self');
        $currentRoute   = $request->route();

        if (Str::contains($self, '/relationships/')) {
            $links['related'] = $this->getContainer()->make('url')->jsonApiRelationship(
                $currentRoute->entityType(),
                $currentRoute->entityId(),
                $currentRoute->relationship(),
                'related'
            );
        }

        return $links;
    }

    /**
     * Get container instance.
     *
     * @return \Illuminate\Contracts\Container\Container
     */
    public function getContainer()
    {
        return $this->container ??= app();
    }

    /**
     * Get top-level included data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return null
     */
    public function getIncluded($request)
    {
        return null;
    }

    /**
     * Get resource meta data.
     *
     * @return array
     */
    public function getApiMeta()
    {
        return [];
    }

    /**
     * Get response HTTP status.
     *
     * @return $this
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set response HTTP status.
     *
     * @param  int  $statusCode
     * @return $this
     */
    public function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * Get response headers.
     *
     * @return $this
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set response headers.
     *
     * @param array $headers
     * @return $this
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
        return $this;
    }
}

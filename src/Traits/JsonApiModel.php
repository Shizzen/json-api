<?php

namespace Shizzen\JsonApi\Traits;

use Illuminate\Support\{
    Arr,
    Str,
    Collection
};
use Illuminate\Database\Eloquent\{
    Relations\Pivot,
    Builder as EloquentBuilder
};
use Throwable;
use Shizzen\JsonApi\Contracts\ApiRequest;
use Shizzen\JsonApi\Events\ResourceOperation;

trait JsonApiModel
{
    use HasBoundRelations;

    /**
     * The bootstrapper of this trait (automatically called).
     *
     * @return void
     */
    protected static function bootJsonApiModel()
    {
        static::created(function ($model) {
            if (config('json-api.broadcasting', false)) {
                broadcast(new ResourceOperation('created', $model));
            }
        });

        static::updated(function ($model) {
            if (config('json-api.broadcasting', false)) {
                broadcast(new ResourceOperation('updated', $model));
            }
        });

        static::deleted(function ($model) {
            if (config('json-api.broadcasting', false)) {
                broadcast(new ResourceOperation('deleted', $model));
            }
        });
    }

    /**
     * The initializer of this trait (automatically called).
     *
     * @return void
     */
    protected function initializeJsonApiModel()
    {
        $this->addObservableEvents([
            'cancelledSave', 'failedSave',
            'cancelledInsert', 'failedInsert',
            'cancelledUpdate', 'failedUpdate',
            'cancelledDelete', 'failedDelete'
        ]);
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     *
     * @throws \Throwable
     */
    public function save(array $options = [])
    {
        try {
            $parentSave = parent::save($options);
            if ($parentSave === false) {
                $this->fireModelEvent('cancelledSave', false);
            }
            return $parentSave;
        }
        catch (Throwable $e) {
            $this->fireModelEvent('failedSave', false);
            throw $e;
        }
    }

    /**
     * Perform a model insert operation.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return bool
     *
     * @throws \Throwable
     */
    protected function performInsert(EloquentBuilder $query)
    {
        try {
            $parentInsert = parent::performInsert($query);
            if ($parentInsert === false) {
                $this->fireModelEvent('cancelledInsert', false);
            }
            return $parentInsert;
        }
        catch (Throwable $e) {
            $this->fireModelEvent('failedInsert', false);
            throw $e;
        }
    }

    /**
     * Perform a model update operation.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return bool
     *
     * @throws \Throwable
     */
    protected function performUpdate(EloquentBuilder $query)
    {
        try {
            $parentUpdate = parent::performUpdate($query);
            if ($parentUpdate === false) {
                $this->fireModelEvent('cancelledUpdate', false);
            }
            return $parentUpdate;
        }
        catch (Throwable $e) {
            $this->fireModelEvent('failedUpdate', false);
            throw $e;
        }
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws \Throwable
     */
    public function delete()
    {
        try {
            $parentDelete = parent::delete();
            if ($parentDelete === false) {
                $this->fireModelEvent('cancelledDelete', false);
            }
            return $parentDelete;
        }
        catch (Throwable $e) {
            $this->fireModelEvent('failedDelete', false);
            throw $e;
        }
    }

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Shizzen\JsonApi\EloquentCollection
     */
    public function newCollection(array $models = [])
    {
        $guessedClass = static::class.'Collection';
        $collectionClass = class_exists($guessedClass) ? $guessedClass : config('json-api.classes.eloquent-collection');
        return new $collectionClass($models);
    }

    /**
     * Router method used to generate this model routes (null cancels generation).
     *
     * @return string|null
     */
    public static function getRouterMethod()
    {
        return config('json-api.router-method');
    }

    /**
     * Get the prefix applied to the model routes.
     *
     * @return string|null
     */
    public static function getRouterPrefix()
    {
        return config('json-api.router-prefix');
    }

    /**
     * Get router settings.
     *
     * @return array
     */
    public static function getRouterSettings()
    {
        return static::$routerSettings ?? [];
    }

    /**
     * The controller class used by this model routes.
     *
     * @return string
     */
    public static function getControllerClass()
    {
        $guessedClass = str_replace('App\\', 'App\\Http\\Controllers\\', static::class) . 'Controller';
        return class_exists($guessedClass) ? $guessedClass : config('json-api.classes.controller');
    }

    /**
     * The resource collection class used to render models.
     *
     * @return string
     */
    public static function getResourceCollectionClass()
    {
        $guessedClass = str_replace('App\\', 'App\\Http\\Resources\\', static::class) . 'Collection';
        return class_exists($guessedClass) ? $guessedClass : config('json-api.classes.resource-collection');
    }

    /**
     * Get model identifiers used by JSON:API.
     *
     * @return array
     */
    public function getApiIdentifiers()
    {
        return [
            'type'  => static::getAlias(),
            'id'    => (string) $this->getKey(),
        ];
    }

    /**
     * Get meta data included into API resources.
     *
     * @return array
     */
    public function getApiMeta()
    {
        return [];
    }

    /**
     * Get links to fetch the model or one of its relationships.
     *
     * @param  string|null  $relationshipName
     * @return array
     */
    public function getApiLinks(?string $relationshipName = null)
    {
        $urlGenerator   = app('url');
        $identifiers    = $this->getApiIdentifiers();
        $otherParams    = $urlGenerator->getRequest()->route()->parameters();

        if ($relationshipName) {
            return [
                'self'      => $urlGenerator->jsonApiRelationship($identifiers['type'], $identifiers['id'], $relationshipName, 'index', $otherParams),
                'related'   => $urlGenerator->jsonApiRelationship($identifiers['type'], $identifiers['id'], $relationshipName, 'related', $otherParams)
            ];
        }

        return [
            'self' => $urlGenerator->route(
                config('json-api.prefix').$identifiers['type'].'.show',
                array_merge(
                    $otherParams,
                    [ Str::singular($identifiers['type']) => $identifiers['id'] ]
                )
            )
        ];
    }

    /**
     * Get attributes according to JSON:API.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getApiAttributes()
    {
        return Collection::make($this->getRelations())
            ->intersectByKeys(array_flip($this->getBoundRelations()))
            ->merge($this->attributesToArray())
            ->reject(function ($value, $attribute) {
                return $this->isHidden($attribute);
            })
            ->transform(function ($relatedModel, $attribute) {
                if (is_object($relatedModel) && Collection::isValid($relatedModel)) {
                    return $relatedModel->mapWithKeys(function ($model, $index) {
                        $boundKeyName = $model->getBoundKeyName();
                        $modelKey = $boundKeyName
                            ? $model->{$boundKeyName}
                            : $index;
                        return [ $modelKey => $model->getApiAttributes() ];
                    });
                }
                return method_exists($relatedModel, 'getApiAttributes')
                    ? $relatedModel->getApiAttributes()
                    : $relatedModel;
            });
    }

    /**
     * Get all the loaded relations for the instance.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getApiRelations()
    {
        return Collection::make($this->getRelations())
            ->reject(function ($relatedModels, $relationshipName) {
                return in_array($relationshipName, $this->getBoundRelations())
                    || $this->isHidden($relationshipName)
                    || $this->getApiAttributes()->has($relationshipName);
            });
    }

    /**
     * Scope a query to sort.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  array  $sorts
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMapSorts(EloquentBuilder $query, array $sorts = [])
    {
        foreach ($sorts as $sort) {
            $field = Str::after($sort, '-');
            $order = Str::startsWith($sort, '-') ? 'DESC' : 'ASC';
            $query->orderBy($field, $order);
        }
        return $query;
    }

    /**
     * Scope a query to filter.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  array  $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMapFilters(EloquentBuilder $query, array $filters = [])
    {
        foreach ($filters as $filterName => $filterValue) {
            $query->where($filterName, $filterValue);
        }
        return $query;
    }

    /**
     * Check whether an attribute or relationship is hidden.
     *
     * @param  string  $target
     * @return bool
     */
    public function isHidden(string $target)
    {
        if (in_array($target, ['id', 'type', $this->getKeyName(), $this->getBoundKeyName()]) || Str::endsWith($target, ['_id', '_type'])) {
            return true;
        }
        if (in_array($target, $this->getHidden())) {
            $hidePredicate = 'hide'.ucfirst($target);
            return method_exists($this, $hidePredicate)
                ? app()->call([$this, $hidePredicate])
                : true;
        }
        $targetRelationship = $this->getRelations()[$target] ?? null;
        return $targetRelationship && $targetRelationship instanceof Pivot;
    }

    /**
     * Get all models from loaded relationships.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getRelatedModels()
    {
        $relatedModels = $this->getApiRelations()->flatten(1);

        $recursiveRelatedModels = $relatedModels->flatMap(function ($relatedModel) {
            return optional($relatedModel)->getRelatedModels();
        });

        return $relatedModels->merge($recursiveRelatedModels)
            ->filter()
            ->unique->getApiIdentifiers()
            ->values();
    }

    /**
     * Save model's relationships
     *
     * @param  iterable  $relatedMapping
     * @return $this
     */
    public function saveRelations(iterable $relatedMapping)
    {
        foreach ($relatedMapping as $relationshipName => $relatedModels) {
            $this->saveRelation($relationshipName, $relatedModels);
        }
        return $this;
    }

    /**
     * Save a model's relationship
     *
     * @param  string  $relationshipName
     * @param  \Illuminate\Database\Eloquent\Model|array  $relatedMapping
     * @return $this
     */
    public function saveRelation(string $relationshipName, $relatedModels)
    {
        $this->{$relationshipName}()->updateRelated($relatedModels);
        return $this;
    }

    /**
     * Get the bound attributes that were changed.
     *
     * @return array
     */
    public function getBoundChanges()
    {
        return Collection::make($this->updatedBound)
            ->map(function ($boundModels, $relationName) {
                if (is_array($boundModels)) {
                    $boundChanges = array_map(
                        function ($model) { return $model->getChanges(); },
                        $boundModels
                    );
                    return array_filter($boundChanges)
                        ? array_map(function ($model) { return $model->getApiAttributes()->toArray(); }, $boundModels)
                        : [];
                }
                else {
                    return $boundModels->getChanges();
                }
            })
            ->filter()
            ->toArray();
    }

    /**
     * Check whether the model got changed as wished.
     *
     * @param  array  $attributes
     * @return bool
     */
    public function wasChangedAsWished(array $attributes)
    {
        return $attributes == array_merge($this->getChanges(), $this->getBoundChanges());
    }

    /**
     * Check whether the model shall be broadcasted onto a public channel.
     *
     * @return bool
     */
    public function publicBroadcast()
    {
        return false;
    }
}

<?php

namespace Shizzen\JsonApi\Traits;

use Closure;
use Shizzen\JsonApi\QueryBuilder;
use Shizzen\JsonApi\EloquentBuilder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

trait HasBoundRelations
{
    /**
     * The bound models which got updated.
     *
     * @var array
     */
    protected $updatedBound = [];

    /**
     * List of fields to retrieve on a select query.
     *
     * @var array
     */
    protected static $retrievedFields = ['*'];

    /**
     * The bootstrapper of this trait (automatically called).
     *
     * @return void
     */
    protected static function bootHasBoundRelations()
    {
        static::saved(function ($model) {
            $model->saveBoundRelations();
        });

        $retrievedFields = array_filter(explode(
            ',',
            app()->make('request')->query('fields', [])[static::getAlias()] ?? ''
        ), 'strlen');

        if ($bound = static::getBoundRelations()) {
            if ($retrievedFields) {
                static::$retrievedFields    = array_diff($retrievedFields, $bound);
                static::$boundRelations     = array_intersect($retrievedFields, $bound);
            }
        }
        elseif ($retrievedFields) {
            static::$retrievedFields = $retrievedFields;
        }
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Shizzen\JsonApi\QueryBuilder  $queryBuilder
     * @return \Shizzen\JsonApi\EloquentBuilder|static
     */
    public function newEloquentBuilder($queryBuilder)
    {
        $eloquentBuilder = app(EloquentBuilder::class, ['query' => $queryBuilder]);
        $queryBuilder->setEloquentBuilder($eloquentBuilder);
        return $eloquentBuilder->with(static::getBoundRelations());
    }

    /**
     * Get a new query builder instance for the connection.
     *
     * @return \Shizzen\JsonApi\QueryBuilder
     */
    protected function newBaseQueryBuilder()
    {
        $connection = $this->getConnection();
        return app(QueryBuilder::class, [
            'connection'    => $connection,
            'grammar'       => $connection->getQueryGrammar(),
            'processor'     => $connection->getPostProcessor(),
        ]);
    }

    /**
     * Get model alias.
     *
     * @return string
     */
    public static function getAlias()
    {
        return (new static)->getTable();
    }

    /**
     * Get fields which are always retrieved (you should include all foreign keys).
     *
     * @return array
     */
    public static function getRetrievedFields()
    {
        return static::$retrievedFields;
    }

    /**
     * Get the attribute name used to index models on a bound relationship.
     *
     * @return string|null
     */
    public function getBoundKeyName()
    {
        return $this->boundKeyName ?? null;
    }

    /**
     * Get the attribute used to index models on a bound relationship.
     *
     * @return mixed|null
     */
    public function getBoundKey()
    {
        $boundKeyName = $this->getBoundKeyName();
        return $boundKeyName ? $this->getAttribute($boundKeyName) : null;
    }

    /**
     * Set the attribute used to index models on a bound relationship.
     *
     * @param  mixed  $key
     * @return $this
     */
    public function setBoundKey($key)
    {
        return $this->setAttribute(
            $this->getBoundKeyName(),
            $key
        );
    }

    /**
     * Get relations which are automatically merged to this model.
     *
     * @return array
     */
    public static function getBoundRelations()
    {
        return static::$boundRelations ?? [];
    }

    /**
     * Save all model's bound relations.
     *
     * @return $this
     */
    public function saveBoundRelations()
    {
        foreach ($this->updatedBound as $relationName => $bound) {
            $relation = $this->{$relationName}();
            if (is_array($bound)) {
                $this->getAttribute($relationName)
                    ->diff($bound)
                    ->each->delete();

                $relation->saveMany($bound);
                $this->setRelation(
                    $relationName,
                    $relation->getRelated()->newCollection($bound)
                );
            }
            elseif ($bound instanceof Closure) {
                $bound();
                unset($this->updatedBound[$relationName]);
            }
            else {
                $relation->save($bound);
                $this->setRelation($relationName, $bound);
            }
        }
        return $this;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        return in_array($key, $this->getBoundRelations())
            ? $this->setBoundAttribute($key, $value)
            : parent::setAttribute($key, $value);
    }

    /**
     * Set a given bound attribute on one of the model's relationships.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return $this
     */
    public function setBoundAttribute(string $key, $value)
    {
        $relationModels = $this->getAttribute($key)
            ?: $this->{$key}()->getRelated()->newInstance();

        if ($relationModels instanceof EloquentCollection) {
            if (is_array($value) && empty($value)) {
                $this->updatedBound[$key] = function () use ($relationModels) {
                    $relationModels->each->delete();
                };
            }
            else {
                foreach ($value as $subKey => $subValue) {
                    $boundModel = optional($relationModels->findByBoundKey($subKey))->fill($subValue)
                        ?: $this->{$key}()->getRelated()->newInstance($subValue)->setBoundKey($subKey);

                    if (array_key_exists($key, $this->updatedBound)) {
                        $this->updatedBound[$key][$subKey] = $boundModel;
                    }
                    else {
                        $this->updatedBound[$key] = [ $subKey => $boundModel ];
                    }
                }
            }
        }
        elseif (is_null($value)) {
            $this->updatedBound[$key] = function () use ($relationModels) {
                $relationModels->delete();
            };
        }
        else {
            $this->updatedBound[$key] = $relationModels->fill($value);
        }

        return $this;
    }
}

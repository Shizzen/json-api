<?php

namespace Shizzen\JsonApi\Traits;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputOption;

trait StubModelParser
{
    /**
     * Build the model replacement values.
     *
     * @param  array  $replace
     * @return array
     */
    protected function buildModelReplacements(array $replace)
    {
        $DummyFullModelClass        = $this->parseModel($this->option('model'));
        $DummyModelClass            = class_basename($DummyFullModelClass);
        $DummyModelVariable         = lcfirst($DummyModelClass);
        $DummyPluralModelVariable   = Str::plural($DummyModelVariable);

        if (! class_exists($DummyFullModelClass)) {
            if ($this->confirm("A {$DummyFullModelClass} model does not exist. Do you want to generate it?", true)) {
                $this->call('json-api:make-model', [
                    'name' => $DummyFullModelClass,
                    '--type' => $DummyPluralModelVariable
                ]);
            }
        }

        return array_merge(
            $replace,
            compact('DummyFullModelClass', 'DummyModelClass', 'DummyModelVariable', 'DummyPluralModelVariable')
        );
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    protected function parseModel($model)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        $model = trim(str_replace('/', '\\', $model), '\\');

        if (! Str::startsWith($model, $rootNamespace = $this->laravel->getNamespace())) {
            $model = $rootNamespace.$model;
        }

        return $model;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array_merge(
            parent::getOptions(),
            [
                ['model', 'm', InputOption::VALUE_OPTIONAL, 'Generate a resource controller for the given model.'],
            ]
        );
    }
}

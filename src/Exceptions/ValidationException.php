<?php

namespace Shizzen\JsonApi\Exceptions;

use Throwable;

class ValidationException extends JsonApiException
{
    /**
     * The path of request payload which went wrong (https://tools.ietf.org/html/rfc6901).
     *
     * @var string
     */
    public $path;

    /**
     * Create a new exception instance.
     *
     * @param  string  $path
     * @param  array  $meta
     * @param  string|null  $message
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(string $path, array $meta = [], ?string $message = null, int $code = 0, ?Throwable $previous = null)
    {
        $this->path = '/'.preg_replace(
            [
                '|~|',
                '|/|',
                '|\.|'
            ],
            [
                '~0',
                '~1',
                '/'
            ],
            $path
        );

        parent::__construct(
            422,
            ['pointer' => $this->path],
            $meta,
            [],
            [],
            $message,
            $code,
            $previous
        );
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return sprintf('%s:%s', parent::getId(), $this->path);
    }
}

<?php

namespace Shizzen\JsonApi\Exceptions;

use ArrayAccess;
use RuntimeException;
use IteratorAggregate;
use Illuminate\Support\Collection;
use Shizzen\JsonApi\Traits\ReportableAndRenderable;

class ExceptionAggregator extends RuntimeException implements ArrayAccess, IteratorAggregate
{
    use ReportableAndRenderable;

    /**
     * The exceptions collection.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $exceptions;

    /**
     * Create a new exception instance.
     *
     * @param  \Illuminate\Support\Collection|array  $exceptions
     */
    public function __construct($exceptions = [])
    {
        $this->exceptions = Collection::wrap($exceptions);

        $messages = $this->exceptions->map->getMessage();

        parent::__construct(
            json_encode($messages->toArray())
        );
    }

    public function offsetSet($offset, $value)
    {
        $this->exceptions->offsetSet($offset, $value);
        $messages = json_decode($this->getMessage());
        $messages[$offset] = $value->getMessage();
        $this->message = json_encode($messages);
    }

    public function offsetExists($offset)
    {
        return $this->exceptions->offsetExists($offset);
    }

    public function offsetUnset($offset)
    {
        $this->exceptions->offsetUnset($offset);
        $messages = json_decode($this->getMessage());
        unset($messages[$offset]);
        $this->message = json_encode($messages);
    }

    public function offsetGet($offset)
    {
        return $this->exceptions->offsetGet($offset);
    }

    /**
     * Allow a "foreach" on this instance.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getIterator()
    {
        return $this->exceptions;
    }

    /**
     * Get a collection wrapping exception data.
     *
     * @return \Illuminate\Support\Collection
     */
    public function collects()
    {
        return $this->exceptions;
    }
}

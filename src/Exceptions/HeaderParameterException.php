<?php

namespace Shizzen\JsonApi\Exceptions;

use Throwable;

class HeaderParameterException extends JsonApiException
{
    /**
     * The request header which got parameters.
     *
     * @var string
     */
    public $header;

    /**
     * Create a new exception instance.
     *
     * @param  string  $header
     * @param  int  $status
     * @param  array  $meta
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(string $header, int $status = 400, array $meta = [], int $code = 0, ?Throwable $previous = null)
    {
        $this->header = $header;

        parent::__construct(
            $status,
            [],
            array_merge(compact('header'), $meta),
            [],
            [],
            null,
            $code,
            $previous
        );
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return sprintf('%s:%s', parent::getId(), $this->header);
    }
}

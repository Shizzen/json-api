<?php

namespace Shizzen\JsonApi\Exceptions;

use Throwable;

class NotAllowedQueryParameterException extends JsonApiException
{
    /**
     * The request query's unknown parameter.
     *
     * @var string
     */
    public $parameter;

    /**
     * Create a new exception instance.
     *
     * @param  string  $parameter
     * @param  array  $meta
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(string $parameter, array $meta = [], int $code = 0, ?Throwable $previous = null)
    {
        $this->parameter = $parameter;

        parent::__construct(
            400,
            array_merge(compact('parameter'), $meta),
            [],
            [],
            [],
            null,
            $code,
            $previous
        );
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return sprintf('%s:%s', parent::getId(), $this->parameter);
    }
}

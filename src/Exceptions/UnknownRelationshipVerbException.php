<?php

namespace Shizzen\JsonApi\Exceptions;

use Throwable;

class UnknownRelationshipVerbException extends JsonApiException
{
	/**
     * The model name.
     *
     * @var string
     */
    public $model;

    /**
     * The relationship name.
     *
     * @var string
     */
    public $relationship;

	/**
     * The unknown verb.
     *
     * @var string
     */
    public $verb;

    /**
     * Create a new exception instance.
     *
     * @param  string  $model
     * @param  string  $relationship
     * @param  string  $verb
     * @param  array  $meta
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(string $model, string $relationship, string $verb, array $meta = [], int $code = 0, ?Throwable $previous = null)
    {
    	$this->model = $model;
    	$this->relationship = $relationship;
        $this->verb = $verb;

        parent::__construct(
            500,
            [],
            array_merge(compact('model', 'relationship', 'verb'), $meta),
            [],
            [],
            null,
            $code,
            $previous
        );
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return sprintf('%s:%s.%s.%s', parent::getId(), $this->model, $this->relationship, $this->verb);
    }
}

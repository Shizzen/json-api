<?php

namespace Shizzen\JsonApi\Exceptions;

use Throwable;

class FileNotFoundException extends JsonApiException
{
    /**
     * The not found file.
     *
     * @var string
     */
    public $file;

    /**
     * Create a new exception instance.
     *
     * @param  string  $file
     * @param  array  $meta
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(string $file, array $meta = [], int $code = 0, ?Throwable $previous = null)
    {
        $this->file = $file;

        parent::__construct(
            404,
            [],
            array_merge(compact('file'), $meta),
            [],
            [],
            null,
            $code,
            $previous
        );
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return sprintf('%s:%s', parent::getId(), $this->file);
    }
}

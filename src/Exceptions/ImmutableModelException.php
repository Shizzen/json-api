<?php

namespace Shizzen\JsonApi\Exceptions;

use Throwable;
use Shizzen\JsonApi\Contracts\JsonApiModel;

class ImmutableModelException extends JsonApiException
{
    /**
     * The immutable model.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    public $model;

    /**
     * Create a new exception instance.
     *
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $model
     * @param  array  $meta
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(JsonApiModel $model, array $meta = [], int $code = 0, ?Throwable $previous = null)
    {
        $this->model = $model;

        parent::__construct(
            403,
            [],
            array_merge($model->getApiIdentifiers(), $meta),
            [],
            [],
            null,
            $code,
            $previous
        );
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        [
            'type'  => $type,
            'id'    => $id,
        ] = $this->model->getApiIdentifiers();

        return sprintf('%s:%s.%s', parent::getId(), $type, $id);
    }
}

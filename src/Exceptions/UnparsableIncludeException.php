<?php

namespace Shizzen\JsonApi\Exceptions;

use Throwable;

class UnparsableIncludeException extends JsonApiException
{
    /**
     * The request query's unknown parameter.
     *
     * @var string
     */
    public $relationship;

    /**
     * Create a new exception instance.
     *
     * @param  string  $relationship
     * @param  array  $meta
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(string $relationship, array $meta = [], int $code = 0, ?Throwable $previous = null)
    {
        $this->relationship = $relationship;

        parent::__construct(
            400,
            ['parameter' => 'include'],
            array_merge(compact('relationship'), $meta),
            [],
            [],
            null,
            $code,
            $previous
        );
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return sprintf('%s:%s', parent::getId(), $this->relationship);
    }
}

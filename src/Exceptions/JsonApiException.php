<?php

namespace Shizzen\JsonApi\Exceptions;

use Shizzen\JsonApi\{
	Traits\Spreadable,
    Traits\ReportableAndRenderable
};
use Throwable;
use Illuminate\Support\Str;
use Illuminate\Contracts\Support\Jsonable;
use Symfony\Component\HttpKernel\Exception\HttpException;

class JsonApiException extends HttpException implements Jsonable
{
	use Spreadable, ReportableAndRenderable;

    /**
     * References to the source of the error.
     *
     * @var array
     */
    public $source;

    /**
     * Non-standard meta-information about the error.
     *
     * @var array
     */
    public $meta;

    /**
     * URIs to help understanding of the error.
     *
     * @var array
     */
    public $links;

    /**
     * Create a new exception instance.
     *
     * @param  int  $statusCode
     * @param  array  $source
     * @param  array  $meta
     * @param  array  $headers
     * @param  array  $links
     * @param  string|null  $message
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(int $statusCode, array $source = [], array $meta = [], array $headers = [], array $links = [], ?string $message = null, int $code = 0, ?Throwable $previous = null)
    {
        $this->source = $source;
        $this->meta = $meta;
        $this->links = $links;
        $this->code = $code;

        parent::__construct(
            $statusCode,
            $message ?: $this->getTitle(),
            $previous,
            $headers,
            $code
        );
    }

    /**
     * Get a short, human-readable summary of the problem.
     *
     * @return string
     */
    public function getTitle()
    {
        $targetConstant = sprintf('%s::E_%d', static::class, $this->getCode());

        return defined($targetConstant) ? constant($targetConstant) : static::getDefaultTitle($this);
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return $this->getTitle();
    }

    /**
     * Format a default title for an exception.
     *
     * @return string
     */
    public static function getDefaultTitle(Throwable $exception)
    {
        return (string) Str::of(class_basename($exception))
            ->beforeLast('Exception')
            ->snake()
            ->replace('_', ' ')
            ->ucfirst();
    }
}

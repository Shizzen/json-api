<?php

namespace Shizzen\JsonApi\Exceptions;

use Throwable;
use Illuminate\Database\Eloquent\Relations\Relation;

class BoundKeyNotFoundException extends JsonApiException
{
    /**
     * The type which does not own any bound key name.
     *
     * @var string
     */
    public $type;

    /**
     * Create a new exception instance.
     *
     * @param  string  $type
     * @param  array  $meta
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(string $type, array $meta = [], int $code = 0, ?Throwable $previous = null)
    {
        $type = Relation::guessType($type);
        $this->type = $type;

        parent::__construct(
            400,
            [],
            array_merge(compact('type'), $meta),
            [],
            [],
            null,
            $code,
            $previous
        );
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return sprintf('%s:%s', parent::getId(), $this->type);
    }
}

<?php

namespace Shizzen\JsonApi\Exceptions;

use Throwable;
use Illuminate\Database\Eloquent\Relations\Relation;

class AttributeNotFoundException extends JsonApiException
{
	/**
     * The unknown attribute.
     *
     * @var string
     */
    public $attribute;

    /**
     * The type which does not own this attribute.
     *
     * @var string
     */
    public $type;

    /**
     * True if the attribute got specified in sparse fieldsets.
     *
     * @var bool
     */
    public $fromHttpQuery;

	/**
     * Create a new exception instance.
     *
     * @param  string  $attribute
     * @param  string  $type
     * @param  bool  $fromHttpQuery
     * @param  array  $meta
     * @param  int  $code
     * @param  \Throwable|null  $previous
     */
    public function __construct(string $attribute, string $type, bool $fromHttpQuery = false, array $meta = [], int $code = 0, ?Throwable $previous = null)
    {
    	$type = Relation::guessType($type);
    	$this->attribute = $attribute;
    	$this->type = $type;
    	$this->fromHttpQuery = $fromHttpQuery;

    	$source = $fromHttpQuery ? ['parameter' => 'fields.'.$type] : [];

        parent::__construct(
            400,
            $source,
            array_merge(compact('attribute', 'type'), $meta),
            [],
            [],
            null,
            $code,
            $previous
        );
    }

    /**
     * Get a unique identifier for this particular instance.
     *
     * @return string
     */
    public function getId()
    {
        return sprintf('%s:%s.%s', parent::getId(), $this->type, $this->attribute);
    }
}

<?php

namespace Shizzen\JsonApi;

use Illuminate\Support\{
    Arr,
    Str,
    Collection,
    ServiceProvider
};
use Illuminate\Routing\Route;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Relations\Relation;

class JsonApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/json-api.php', 'json-api'
        );

        $this->app->when(Http\Middleware\VerifyJsonApi::class)
            ->needs('$queryParameters')
            ->give(function ($app) {
                return $app['config']->get('json-api.media.allowed');
            });

        $this->app->bindIf(Contracts\ApiRequest::class, function ($app) {
            $routeModel = $app['request']->route()->entityClass(false);

            $guessedClass = str_replace(config('json-api.models-namespace'), 'App\\Http\\Requests\\', $routeModel) . 'Request';

            return $app[
                class_exists($guessedClass) ? $guessedClass : Http\Requests\ApiRequest::class
            ];
        }, true);

        $this->app->resolving(Contracts\ApiRequest::class, function ($request, $app) {
            $oldRequest = $app['request'];

            get_class($oldRequest)::createFrom($oldRequest, $request);

            $app->instance('request', $request);
            $app->alias(Contracts\ApiRequest::class, get_class($request));

            $app->bind(Contracts\JsonApiModel::class, function ($app) use ($request) {
                return $request->route()->entityClass(true);
            });
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->commands([
            Commands\PolicyMakeCommand::class,
            Commands\ControllerMakeCommand::class,
            Commands\ModelMakeCommand::class,
            Commands\RequestMakeCommand::class,
            Commands\ExceptionMakeCommand::class,
        ]);

        $config = $this->app['config'];

        $this->app['router']->middlewareGroup(
            'json-api', $config->get('json-api.middleware', [])
        );

        if ($config->get('json-api.broadcasting', false)) {
            require_once __DIR__.'/Broadcasting/channels.php';
        }

        $this->loadMixins();

        $this->loadTranslationsFrom(__DIR__.'/../lang', 'json-api');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/json-api.php' => config_path('json-api.php'),
            ], 'json-api-config');

            $this->publishes([
                __DIR__.'/../lang' => resource_path('lang/vendor/json-api'),
            ], 'json-api-lang');
        }
    }

    /**
     * Load macros from mixins.
     *
     * @return void
     */
    protected function loadMixins()
    {
        Arr::mixin(new Mixins\Arr);

        Str::mixin(new Mixins\Str);

        Collection::mixin(new Mixins\Collection);

        Relation::mixin(new Mixins\Relation);

        $responseClass = get_class($this->app[ResponseFactory::class]);
        $responseClass::mixin(new Mixins\Response);

        Route::mixin(new Mixins\Route);

        $routerClass = get_class($this->app['router']);
        $routerClass::mixin(new Mixins\Router);

        $urlGenClass = get_class($this->app['url']);
        $urlGenClass::mixin(new Mixins\UrlGenerator);
    }
}

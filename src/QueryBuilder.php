<?php

namespace Shizzen\JsonApi;

use Illuminate\Database\Query\Builder;

class QueryBuilder extends Builder
{
    use Traits\QueryBuilder;
}

<?php

use Shizzen\JsonApi\Broadcasting;
use Illuminate\Support\Facades\Broadcast;

Broadcast::channel('json-api.{resourceType}', Broadcasting\JsonApiChannel::class);

Broadcast::channel('json-api.{resourceType}.{resourceId}', Broadcasting\JsonApiChannel::class);

Broadcast::channel('related.{userType}.{userId}.{resourceType}', function (App\User $user, string $userType, string $userId, string $resourceType) {
    return (string) $user->getKey() === (string) $userId;
});

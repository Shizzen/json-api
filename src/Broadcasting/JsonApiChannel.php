<?php

namespace Shizzen\JsonApi\Broadcasting;

use Illuminate\Contracts\Auth\Access\Authorizable;
use Illuminate\Database\Eloquent\Relations\Relation;

class JsonApiChannel
{
    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Authorizable  $user
     * @return array|bool
     */
    public function join(Authorizable $user, string $resourceType, $resourceId = null)
    {
        $target = $resourceId
            ? Relation::parseIdentifier($resourceType, $resourceId)
            : Relation::getMorphedModel($resourceType);

        return $user->can(
            is_string($target) ? 'index' : 'show',
            $target
        );
    }
}

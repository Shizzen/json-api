<?php

namespace Shizzen\JsonApi\Events;

use Illuminate\Broadcasting\{
    Channel,
    PrivateChannel,
    InteractsWithSockets
};
use Shizzen\JsonApi\Contracts\JsonApiModel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ResourceOperation implements ShouldBroadcast
{
    use InteractsWithSockets;

    /**
     * Resource id and type
     *
     * @var array
     */
    protected $identifiers;

    /**
     * Resource attributes
     *
     * @var array
     */
    protected $attributes;

    /**
     * Operation applied to the resource (used as channel name)
     *
     * @var string
     */
    protected $operation;

    /**
     * Broadcast channels list
     *
     * @var array
     */
    protected $channels;

    /**
     * Create a new event instance.
     *
     * @param  string  $operation
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $resource
     */
    public function __construct(string $operation, JsonApiModel $resource)
    {
        $this->identifiers  = $resource->getApiIdentifiers();
        $this->attributes   = $resource->getApiAttributes()->toArray();
        $this->operation    = $operation;

        [
            'type'  => $resourceType,
            'id'    => $resourceId
        ] = $this->identifiers;

        $channelType = $resource->publicBroadcast() ? Channel::class : PrivateChannel::class;

        $channels = [
            new $channelType("json-api.${resourceType}"),
            new $channelType("json-api.${resourceType}.${resourceId}")
        ];

        if ($users = $resource->users ?? null) {
            $channels = array_merge(
                $channels,
                $users->map([$this, 'getUserRelatedChannel'])->all()
            );
        }
        elseif ($user = $resource->user ?? null) {
            $channels[] = $this->getUserRelatedChannel($user);
        }

        $this->channels = $channels;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|\Illuminate\Broadcasting\Channel[]
     */
    public function broadcastOn()
    {
        return $this->channels;
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return $this->operation;
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return array_merge(
            $this->identifiers,
            [
                'attributes' => $this->attributes
            ]
        );
    }

    /**
     * Create a new private channel of a resource related to a user.
     *
     * @param  \Shizzen\JsonApi\Contracts\JsonApiModel  $user
     * @return \Illuminate\Broadcasting\PrivateChannel
     */
    protected function getUserRelatedChannel(JsonApiModel $user)
    {
        [ 'type' => $resourceType ] = $this->identifiers;

        [
            'type'  => $userType,
            'id'    => $userId
        ] = $user->getApiIdentifiers();

        return new PrivateChannel("related.${userType}.${userId}.${resourceType}");
    }
}

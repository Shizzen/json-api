<?php

namespace Shizzen\JsonApi;

use Illuminate\Database\Eloquent\Collection;

class EloquentCollection extends Collection
{
    use Traits\EloquentCollection;
}
